<?
namespace Roxot\FootballMaster;

class FootballMatchPlayer
{
    protected $goalsNum = 0;
    protected $goalPassNum = 0;
    protected $yellowCardNum = 0;
    protected $redCardNum = 0;

    private $name;
    private $number;
    private $gameTime = 0;

    /**
     * @param $arPlayer
     */
    function __construct($arPlayer) {
        $this->name = $arPlayer["name"];
        $this->number = $arPlayer["number"];
    }

    /**
     * @param $property
     * @return null|$value
     */
    function __get($property) {
        if (!property_exists($this, $property)) {
            return null;
        }

        switch ($property) {
            default:
                $value = $this->$property;
                return $value;
                break;
        }
    }

    /**
     * For twig
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * For twig
     * @return int
     */
    public function getGoalsNum() {
        return $this->goalsNum;
    }

    /**
     * For twig
     * @return int
     */
    public function getGoalPassNum() {
        return $this->goalPassNum;
    }

    /**
     * For twig
     * @return int
     */
    public function getGameTime() {
        return $this->gameTime;
    }

    /**
     * For twig
     * @return int
     */
    public function getYellowCardNum() {
        return $this->yellowCardNum;
    }

    /**
     * For twig
     * @return int
     */
    public function getRedCardNum() {
        return $this->redCardNum;
    }

    /**
     * For twig
     * @return mixed
     */
    public function getNumber() {
        return $this->number;
    }


    /**
     * @param $time
     */
    protected function setGameTime($time) {
        $this->gameTime = $time;
    }
}