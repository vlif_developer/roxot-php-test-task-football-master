<?
namespace Roxot\FootballMaster;

class FootballMatch extends FootballMatchTeam
{
    public $arStadium = [];

    private $arHistory = [];
    private $result = [];
    private $arOFootballMatchTeams = [];
    //private $arReplaces = [];

    private $resultHtmlFileName;

    /**
     * @param $fileName
     */
    function __construct($fileName) {
        $this->resultHtmlFileName = explode(".", $fileName)[0].".html";
    }


    /**
     * @return string
     */
    public function getResultHtmlFileName() {
        return $this->resultHtmlFileName;
    }

    /**
     * @return array
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @return array
     */
    public function getArOFootballMatchTeams() {
        return $this->arOFootballMatchTeams;
    }

    /**
     * For twig
     * @return array
     */
    public function getArHistory() {
        return $this->arHistory;
    }


    /**
     * @param $arRecord
     */
    public function writeToHistory($arRecord) {
        $this->arHistory[] = $arRecord;
    }

    /**
     * @param $team
     */
    public function changeResultMatch($team) {
        if (!isset($this->result[$team])) {
            $this->result[$team] = 0;
        }

        $this->result[$team]++;
    }

    /**
     * @param FootballMatchTeam $oFootballMatchTeam
     */
    public function addOFootballMatchTeam(FootballMatchTeam $oFootballMatchTeam) {
        $this->arOFootballMatchTeams[$oFootballMatchTeam->title] = $oFootballMatchTeam;
    }

    /**
     * @param $teamTitle
     * @param $playerNumber
     */
    public function addGoal($teamTitle, $playerNumber) {
        $this->arOFootballMatchTeams[$teamTitle]
            ->arOFootballMatchPlayers[$playerNumber]
            ->goalsNum++;
    }
    /**
     * @param $teamTitle
     * @param $assistantPlayerNumber
     */
    public function addGoalPass($teamTitle, $assistantPlayerNumber) {
        $this->arOFootballMatchTeams[$teamTitle]
            ->arOFootballMatchPlayers[$assistantPlayerNumber]
            ->goalPassNum++;
    }

    /**
     * @param $cardType
     * @param $teamTitle
     * @param $playerNumber
     */
    public function addCard($cardType, $teamTitle, $playerNumber) {
        if ("yellow" == $cardType) {
            $this->arOFootballMatchTeams[$teamTitle]
                ->arOFootballMatchPlayers[$playerNumber]
                ->yellowCardNum++;
            $yellowCardNum =  $this->arOFootballMatchTeams[$teamTitle]
                ->arOFootballMatchPlayers[$playerNumber]
                ->yellowCardNum;
            if (2 == $yellowCardNum) {
                $this->arOFootballMatchTeams[$teamTitle]
                    ->arOFootballMatchPlayers[$playerNumber]
                    ->redCardNum++;
            }
        } elseif("red" == $cardType) {
            $this->arOFootballMatchTeams[$teamTitle]
                ->arOFootballMatchPlayers[$playerNumber]
                ->redCardNum++;
        }
    }

    /**
     * @param $teamTitle
     * @param $inPlayerNumber
     * @param $outPlayerNumber
     * @param $time
     */
    public function addReplace($teamTitle, $inPlayerNumber, $outPlayerNumber, $time) {
        $this->arOFootballMatchTeams[$teamTitle]->arReplaces[$outPlayerNumber] = [
            "IN" => $inPlayerNumber,
            "OUT" => $outPlayerNumber,
            "TIME" => $time
        ];
    }

    /**
     * @param $footballMatchTime
     */
    public function calculateTimeGamePlayers($footballMatchTime) {
        foreach ($this->arOFootballMatchTeams as $oFootballMatchTeam) {
            $teamTitle = $oFootballMatchTeam->title;
            $arStartPlayerNumbers = $oFootballMatchTeam->arStartPlayerNumbers;
            $arReservePlayerNumbers = $oFootballMatchTeam->arReservePlayerNumbers;

            foreach ($arStartPlayerNumbers as $playerNumber) {
                // Full time
                if (!isset($oFootballMatchTeam->arReplaces[$playerNumber])) {
                    $this->arOFootballMatchTeams[$teamTitle]
                        ->arOFootballMatchPlayers[$playerNumber]->setGameTime($footballMatchTime);
                // Replace out
                } else {
                    $this->arOFootballMatchTeams[$teamTitle]
                        ->arOFootballMatchPlayers[$playerNumber]->setGameTime(
                            $oFootballMatchTeam->arReplaces[$playerNumber]["TIME"]
                        );
                }
            }

            // Replace in
            foreach ($arReservePlayerNumbers as $playerNumber) {
                foreach ($oFootballMatchTeam->arReplaces as $arReplace) {
                    if ($arReplace["IN"] != $playerNumber) {
                        continue;
                    }

                    if (!isset($oFootballMatchTeam->arReplaces[$arReplace["IN"]])) {
                        $this->arOFootballMatchTeams[$teamTitle]
                            ->arOFootballMatchPlayers[$playerNumber]
                            ->setGameTime($footballMatchTime-$arReplace["TIME"]);
                    } else {
                        $this->arOFootballMatchTeams[$teamTitle]
                            ->arOFootballMatchPlayers[$playerNumber]
                            ->setGameTime($oFootballMatchTeam->arReplaces[$arReplace["IN"]]["TIME"]-$arReplace["TIME"]);
                    }
                }
            }
        }
    }
}