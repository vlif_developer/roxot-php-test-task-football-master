<?
namespace Roxot\FootballMaster;

class FootballMatchTeam extends FootballMatchPlayer
{
    protected $arOFootballMatchPlayers = [];
    protected $arReplaces = [];

    private $title;
    private $country;
    private $coach;
    private $arStartPlayerNumbers = [];
    private $arReservePlayerNumbers = [];

    /**
     * @param $arTeam
     */
    function __construct($arTeam) {
        $this->title = $arTeam["title"];
        $this->country = $arTeam["country"];
        $this->coach = $arTeam["coach"];
        foreach ($arTeam["startPlayerNumbers"] as $playerNumber) {
            $this->arStartPlayerNumbers[$playerNumber] = $playerNumber;
        }

        // Players
        foreach ($arTeam["players"] as $arPlayer) {
            $oFootballMatchPlayer = new FootballMatchPlayer($arPlayer);
            $this->addOFootballMatchPlayer($oFootballMatchPlayer);

            // Reserve players
            if (!isset($this->arStartPlayerNumbers[$arPlayer["number"]])) {
                $this->arReservePlayerNumbers[] = $arPlayer["number"];
            }
        }
    }

    /**
     * @param $property
     * @return null|$value
     */
    function __get($property) {
        if (!property_exists($this, $property)) {
            return null;
        }

        switch ($property) {
            default:
                $value = $this->$property;
                return $value;
                break;
        }
    }

    /**
     * For twig
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * For twig
     * @return array
     */
    public function getArStartPlayerNumbers() {
        return $this->arStartPlayerNumbers;
    }

    /**
     * For twig
     * @return array
     */
    public function getArReservePlayerNumbers() {
        return $this->arReservePlayerNumbers;
    }

    /**
     * For twig
     * @return array
     */
    public function getArOFootballMatchPlayers() {
        return $this->arOFootballMatchPlayers;
    }

    /**
     * For twig
     * @return array
     */
    public function getArReplaces() {
        return $this->arReplaces;
    }


    /**
     * @param FootballMatchPlayer $oFootballMatchPlayer
     */
    public function addOFootballMatchPlayer(FootballMatchPlayer $oFootballMatchPlayer) {
        $this->arOFootballMatchPlayers[$oFootballMatchPlayer->number] = $oFootballMatchPlayer;
    }
}