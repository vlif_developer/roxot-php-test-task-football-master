<?
namespace Roxot\FootballMaster;


//set_error_handler(function() { /* ignore errors */ });
set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
});


class FootballMatchGeneratorHtmlPages
{
    private $sourcePath = "";
    private $resultPath = "";
    private $arOFootbalMatches = [];

    /**
     * @param string $sourcePath
     * @param string $resultPath
     */
    function __construct($sourcePath="/source/matches/", $resultPath="/result/") {
        $this->sourcePath = $sourcePath;
        $this->resultPath = $resultPath;
    }


    public function generateHtmlPages() {
        $this->parseJsonFiles();

        $this->formHtmlPages($this->arOFootbalMatches);
    }

    public function parseJsonFiles() {
        $arFootballMatchFiles = scandir($_SERVER["DOCUMENT_ROOT"].$this->sourcePath);
        foreach ($arFootballMatchFiles as $fileName):
            if ("." == $fileName || ".." == $fileName) { continue; }
            $arFileName = explode(".", $fileName);
            if ("json" != $arFileName[count($arFileName)-1]) { continue; }

            $file = file_get_contents($_SERVER["DOCUMENT_ROOT"] . $this->sourcePath . $fileName);
            $arFootballMatchHistory = json_decode($file, true);
            if (!is_array($arFootballMatchHistory)) {
                continue;
            }

            try {

                $oFootballMatch = new FootballMatch($fileName);

                $finishPeriodCntr = 0;
                foreach ($arFootballMatchHistory as $arRecord):

                    switch ($arRecord["type"]):
                        case "info":

                            //$oFootballMatch->writeToHistory($arRecord);

                            break;
                        case "startPeriod": // important event

                            //$oFootballMatch->writeToHistory($arRecord);

                            // stadium
                            if (!empty($arRecord["details"]["stadium"])) {
                                $oFootballMatch->arStadium = $arRecord["details"]["stadium"];
                            }

                            // teams
                            if (!empty($arRecord["details"]["team1"])) {
                                $arTeam1 = $arRecord["details"]["team1"];
                                $oFootballMatchTeam = new FootballMatchTeam($arTeam1);
                                $oFootballMatch->addOFootballMatchTeam($oFootballMatchTeam);
                            }
                            if (!empty($arRecord["details"]["team2"])) {
                                $arTeam2 = $arRecord["details"]["team2"];
                                $oFootballMatchTeam = new FootballMatchTeam($arTeam2);
                                $oFootballMatch->addOFootballMatchTeam($oFootballMatchTeam);
                            }

                            break;
                        case "dangerousMoment": // important event

                            $oFootballMatch->writeToHistory($arRecord);

                            break;
                        case "yellowCard": // important event

                            $oFootballMatch->writeToHistory($arRecord);

                            $oFootballMatch->addCard(
                                "yellow",
                                $arRecord["details"]["team"],
                                $arRecord["details"]["playerNumber"]
                            );

                            break;
                        case "redCard": // important event

                            $oFootballMatch->writeToHistory($arRecord);

                            $oFootballMatch->addCard(
                                "red",
                                $arRecord["details"]["team"],
                                $arRecord["details"]["playerNumber"]
                            );

                            break;
                        case "goal": // important event

                            $oFootballMatch->writeToHistory($arRecord);

                            $oFootballMatch->changeResultMatch($arRecord["details"]["team"]);

                            $oFootballMatch->addGoal(
                                $arRecord["details"]["team"],
                                $arRecord["details"]["playerNumber"]
                            );
                            if (intval($arRecord["details"]["assistantNumber"]) > 0) {
                                $oFootballMatch->addGoalPass(
                                    $arRecord["details"]["team"],
                                    $arRecord["details"]["assistantNumber"]
                                );
                            }

                            break;
                        case "finishPeriod": // important event
                            $finishPeriodCntr++;

                            $oFootballMatch->writeToHistory($arRecord);

                            if (2 == $finishPeriodCntr) {
                                $oFootballMatch->calculateTimeGamePlayers($arRecord["time"]);
                            }

                            break;
                        case "replacePlayer": // important event

                            $oFootballMatch->writeToHistory($arRecord);

                            $oFootballMatch->addReplace(
                                $arRecord["details"]["team"],
                                $arRecord["details"]["inPlayerNumber"],
                                $arRecord["details"]["outPlayerNumber"],
                                $arRecord["time"]
                            );

                            break;
                        default:
                            break;
                    endswitch;

                endforeach;

                $this->addOFootbalMatch($oFootballMatch);

            } catch(\ErrorException $e) {

                continue;

            }

        endforeach;
    }

    /**
     * @param FootballMatch $oFootballMatch
     */
    private function addOFootbalMatch(FootballMatch $oFootballMatch) {
        $this->arOFootbalMatches[] = $oFootballMatch;
    }

    private function formHtmlPages($arOFootbalMatches) {
        $loader = new \Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"]."/local/templates");
        $twig = new \Twig_Environment($loader);
        $footballMatchResTmpl = $twig->loadTemplate("football_match_result.tmpl");

        foreach ($arOFootbalMatches as $oFootballMatch) {
            $arOFootballMatchTeams = $oFootballMatch->getArOFootballMatchTeams();
            $arTeamsTitle = array_keys($arOFootballMatchTeams);
            $footballMatchResHtmlText = $footballMatchResTmpl->render(array(
                "oFootballMatch" => $oFootballMatch,
                "team1" => $arOFootballMatchTeams[$arTeamsTitle[0]],
                "team2" => $arOFootballMatchTeams[$arTeamsTitle[1]]
            ));

            $fileName = $oFootballMatch->getResultHtmlFileName();
            $fp = fopen($_SERVER["DOCUMENT_ROOT"].$this->resultPath.$fileName, "w");
            fwrite($fp, $footballMatchResHtmlText);
            fclose($fp);
        }
    }
}